
## Version 1.1.0

* Make compatible with tidyr version 1.0.0 (uses `(un)nest_legacy`)
* Improve generation calculation by integrating over the time segment between two growth observations.

## Version 1.0.5

* Added plots on dilution rate, flow rate and volume changes
* Online view of intermediate data.frames used for plotting
* Improved layout
* Online view of README.md and CHANGELOG.md

## Version 1.0.4

* Simplify decision to measurement matching in turbidostat data (rely on measurement_id)
* Add more information on temporal scale of experiment by showing time of first measurement

## Version 1.0.3

* Update to mmpr 0.6.1
* Add Least Squares method
