# Turbidostat app

This shiny app helps users with analyzing turbidostat data, it can also be used as a dashboard to follow experiments in real-time.

## Features

* Insight in OD and Light settings
* Download of raw data
* Estimation of growth rates from OD data.
* Estimation of dilution rates from OD data.
* Estimation of number of generations from growth rate, dilution rate and volume changes.

### Growth Rates

For every dilution made the growth rate is estimated by calculating the slope of the log transformed OD data.

1. $x_t = x_0 * e^{\mu * t}$
2. $log(\frac{x_t}{x_0}) = \mu * t$
3. $log(x_t) = \mu * t - log(x_0)$

Equation 3 is a general linear expression, of which $\mu$ is the slope.
Several regression methods are available calculate regression and estimate the slope, such as:

* `Least Squares`: A fast method using the correlation coefficient to estimate the slope.
* `Linear Models`: A more expensive method using `R`'s built-in linear models.
* `Theil Sein`: A method more robust against noise by calculating the median slope (instead of mean).

To improve the quality of the fits on noisy data, the regression is only applied to the points *between* the lowest and highest point:

```{r}
data %>%
  filter(
    row_number() >= which.min(OD,
    row_number() <= which.max(OD)
  )
```

This means that OD values *before* the minimum and *after* the maximum are ignored. This procedure does not affect the estimated growth rate on "perfect" curves (i.e. straight line), but will increase the estimated growth rate for curves with a "lag" or "stationary" phase.

#### Generations from growth rates

The number of generations can be calculated as follows:

1. $\mu = \frac{ln(2)}{T}$
2. $T = \frac{t}{G} = \frac{ln(2)}{\mu}$
3. $G = \frac{t}{\frac{ln(2)}{\mu}}$

* $\mu$ the observed growth rate.
* $T$ is the doubling time.
* $t$ the time over which the growth rate occurred.
* $G$ the number of generations.

### Dilution Rates

The dilution rate is estimated from the drop in OD observed after each dilution event. 
This in turn is used to calculate the volume that had to be added to the culture to achieve this drop in OD.

*NOTE: This method is sensitive to outliers in OD measurements as it uses the maximum and minimum OD observed in each dilution cycle.*

#### Generations from dilution rates

The number of generations is calculated from the dilution rate as follows:

1. $d = \frac{ln(2)}{T}$ 
2. $T = \frac{t}{G} = \frac{ln(2)}{d}$
3. $G = \frac{t}{\frac{ln(2)}{d}}$

* $d$ the opbserved dilution rate.
* $T$ is the doubling time.
* $t$ the time over which the growth rate occurred.
* $G$ the number of generations.

### Volume Changes

When knowning the dilution achieved with every dilution event and the volume of the vessel, one can calculate the volume added.
By knowing the total amount of volume added, we can calculate the number of volume changes (i.e. how often did we dilute by 2).

So we calculate the number of volume changes ($V_c$) as the number of times the vessel volume ($V_v$) was pumped into the vessel $V_p$.

* $V_c = \frac{V_p}{V_v}$

NOTE: Volume change is **NOT** equal to generations as this is not in log space.

## Usage

## Installation

### Requirements

* shiny
* tidyverse (tidyr >= 1.0.0)
* broom
* mmpr (https://gitlab.com/mmp-uva/mmpr)

### Data access

Data files should be placed in a folder called `data` and made accessible to the user executing this shiny app.
pyCultivator data files are automatically recognised and loaded.
