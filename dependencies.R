# These libraries are the dependencies of the cultivation_app
#
# Author: Joeri Jongbloets <j.a.jongbloets@uva.nl>
# Author: Hugo Pineda <hugo.pinedahernandez@student.uva.nl>
#

# slider: for sliding windows
require(slider)
## mmpr: for data loading
require(mmpr)
# tidyverse: for dplyr and ggplot
require(tidyverse)
# broom: for extracting estimates and p-values
require(broom)
## readr: for reading and writing
# library(readr)
# tidyr: for spread and gather
# require(tidyr)
# purrr: for functional programming
require(purrr)
# dplyr: for data wrangling
# require(dplyr)
# future: multiprocessing
require(future)
plan("multiprocess")