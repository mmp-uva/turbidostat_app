# 
# Server logic of the cultivation app
# Here all the input is processed and output generated.
# Most of the tasks are delegated to functions from global.R
#
# Author: Joeri Jongbloets <j.a.jongbloets@uva.nl>
# Author: Hugo Pineda <hugo.pinedahernandez@student.uva.nl>
#

library(shiny)
require(knitr)

shinyServer(function(input, output, session) {
  
  # keeps track of all experiments
  experiments <- reactive({
    input$reload.data
    
    experiments <- find_pycultivator_sqlite(
      data.base.dir, prefix=data.file.prefix
    )  %>%
      arrange(desc(t_zero))
    
    if (nrow(experiments) > 0) {
      pn <- isolate(input$project.name)
      selected <- first(experiments$name)
      if (pn %in% experiments$name) {
        selected <- pn
      }
      if (nrow(experiments) > 0) {
        # update project.name list
        updateSelectInput(
          session, "project.name",
          choices = experiments$name,
          selected = selected
        )
      }
    }
    
    experiments
  })
  
  experiment <- reactive({
    df <- data.frame()
    
    if (nrow(experiments()) > 0) {
      df <- experiments() %>% 
        filter(name == input$project.name)
    }
    
    df
  })
  
  ## Declare reactive values based on input
  od.led <- reactive({
    validate(
      need(input$od.led.select != "", "Please choose at least one LED wavelength.")
    )
    input$od.led.select
  })
  od.raw <- reactive({
    input$od.raw.select
  })
  od.log <- reactive({
    input$od.log.select
  })
  xlim <- reactive({
    input$ranges
  })
  xlim.zoom <- reactive({
    input$time.filter.zoom == TRUE
  })
  growth.method <- reactive({
    input$growth.method
  })
  growth.exclude.first <- reactive({
    input$growth.exclude.first
  })
  growth.exclude.last <- reactive({
    input$growth.exclude.last
  })
  
  channel <- reactive({
    if (input$channel.select) {
      validate(
        need(input$channel != "", "Please choose at least one channel.")
      )
    }
    input$channel
  })
  
  ## Reactive od data
  data.tb <- reactive({
    
    df.result <- data.frame()
    
    # load experiment data
    project.name <- input$project.name
    # e <- experiments() %>% filter( name == project.name)
    
    validate(
      need(nrow(experiment()) > 0, "No experiment files found")
    )
    
    fp <- experiment()$path[[1]]
    ft <- experiment()$format[[1]]
    
    validate(
      need(file.exists(fp), glue::glue("Unable to open {fp}")),
      need(ft %in% c("db", "sqlite"), glue::glue("{ft} is not supported!"))
    )
    
    df.od <- load_pycultivator_sqlite(fp)
    df.tb <- load_turbidostat_sqlite(fp)
    
    validate(
      need(nrow(df.od) > 0, "No OD Data in this file"),
      need(nrow(df.tb) > 0, "No Turbidostat data in this file")
    )
    
    ## obtain decision information
    # df.decisions <- df.od %>%
    #   filter(od_led == 720) %>% 
    #   combine_turbidostat(df.tb) %>%
    #   na.omit() %>%
    #   ungroup() %>%
    #   select(experiment_id, channel, time_h, decision, pump)
    
    # combine with od data at 5 minute intervals
    df.result <- df.od %>%
      combine_turbidostat(df.tb) %>%
      # set NA values in pump to FALSE
      mutate(
        # decision = ifelse(is.na(decision), lag(decision, default = 0), decision),
        pump = ifelse(is.na(pump), F, as.logical(pump))
      )
  
  if (growth.exclude.first()) {
    df.result <- df.result %>%
      group_by(channel, decision) %>%
      filter( OD != first(OD))
  }
  
  if (growth.exclude.last()) {
    df.result <- df.result %>%
      group_by(channel, decision) %>%
      filter( OD != last(OD))
  }
    
    validate( need( nrow(df.result) > 0, "No data to show" ))
    
    df.result
  })
  
  ## Reactive filtered od data
  data.tb.f <- reactive({
    df <- data.tb()
    validate(
      need(nrow(df) > 0, "No data to show")
    )
    
    ## generate filters
    filters <- list(
      xlim = xlim(),
      xlim.zoom = xlim.zoom() == TRUE,
      filter.channel = input$channel.select == TRUE,
      channels = if(input$channel.select == TRUE) as.list(channel()) else list(NA),
      od.led = od.led(),
      od.raw = od.raw(),
      od.log = od.log()
    )
    ## filter data
    df <- filter.data(df, filters)
    
    df
  })
  
  ## Growth calculation
  data.growth <- reactive({
    df.growth <- data.frame()
    
    ## generate filters
    filters <- list(
      # xlim = xlim(),
      # xlim.zoom = xlim.zoom() == TRUE,
      # filter.channel = input$channel.select == TRUE,
      # channels = if(input$channel.select == TRUE) as.list(channel()) else list(NA),
      od.raw = od.raw()
    )
    
    df <- data.tb() %>%
      filter(od_led == 720) %>%
      # filter.channels(filters) %>%
      # filter.xlim(filters) %>%
      mutate.od.raw(filters) %>%
      filter.od.na()
    
    validate(
      need(nrow(df) > 0, "No data to calculate growth rates from")
    )
    
    method <- growth.method()
    
    df.growth <- df %>% 
      filter(!is.na(measurement_id) & !is.na(decision)) %>%
      filter(!is.na(log(OD)) & !is.infinite(log(OD))) %>%
      group_by(channel) %>% 
      nest_legacy() %>%
      mutate(
        rates = map(data, function(d) { future(turbidostat_growth(d, time_h, OD, method=method)) }),
        rates = values(rates)
      ) %>%
      unnest_legacy(rates) %>%
      mutate(method = method)
    
    df.result <- left_join(
      df %>% group_by(channel, decision) %>% arrange(-time_h) %>% slice(1),
      df.growth %>% select(-time_h),
      by = c("channel", "decision")
    ) %>% 
      na.omit()
    
    validate( need( nrow(df.result) > 0, "No data to show"))
    
    df.result
  })
  
  data.dilution <- reactive({
    
    df.dilution <- data.frame()
    
    ## generate filters
    filters <- list(
      # xlim = xlim(),
      # xlim.zoom = xlim.zoom() == TRUE,
      # filter.channel = input$channel.select == TRUE,
      # channels = if(input$channel.select == TRUE) as.list(channel()) else list(NA),
      od.raw = od.raw()
    )
    
    df.tb <- data.tb() %>%
      filter(od_led == 720) %>%
      # filter.channels(filters) %>%
      # filter.xlim(filters) %>%
      mutate.od.raw(filters) %>%
      filter.od.na()
    
    df.dilution <- df.tb %>%
      group_by(channel, decision) %>%
      nest() %>%
      arrange(decision) %>%
      group_by(channel) %>%
      mutate(
        time_h = map_dbl(data, function(d) {
          max(d$time_h)
        }),
        time_h_d = time_h - lag(time_h, default = min(time_h)),
        OD_max = map_dbl(data, function(d) {
          max(d$OD)
        }),
        OD_min = map_dbl(data, function(d) {
          min(d$OD)
        }),
        # % diluted: from 1 to 0.75 = 0.25
        dilution = 1 - (lead(OD_min) / OD_max),
        dilution_rate = dilution / time_h_d
      ) %>%
      filter(
        !is.na(dilution), time_h_d > 0, dilution_rate > 0
      )
      
    df.dilution %>% select(-data)
  })
  
  data.pump <- reactive({
    
    df.pump <- data.frame()
    
    vessel_volume <- input$vessel.volume
    
    df.pump <- data.dilution() %>%
      group_by(channel) %>%
      arrange(decision) %>%
      mutate(
        pump_volume = vessel_volume * ((1/(1-dilution)) - 1),
        flow_rate = pump_volume / (time_h_d * 60) # ml / min
      ) %>%
      filter(
        pump_volume > 0
      ) %>%
      mutate(
        total_pump_volume = accumulate(pump_volume, `+`),
        volume_changes = total_pump_volume / vessel_volume
      )
    
    df.pump
  })
  
  ## Event handler for the zoom
  obs.click <- observeEvent(input$zoom_dblclick, {
    data <- data.tb()
    if (nrow(data) > 0) {
      brush <- input$zoom_brush
      xlim.new <- c(min(data$time_h), max(data$time_h))
      if (!is.null(brush)) {
        xlim.new <- c(brush$xmin, brush$xmax)
      }
      updateSliderInput(session = session, "ranges", value=c(xlim.new[[1]], xlim.new[[2]] ))
    }
  })
  
  ## Controls
  
  ## Show time filter when there is data available
  output$time.filter <- renderUI({ 
    data <- data.tb()
    if (nrow(data) > 0) {
      tagList(
        checkboxInput("time.filter.zoom",
                      label = "Filter instead of zoom",
                      value = FALSE),
        helpText("Note: Filtering affects the y-axis scale."),
        sliderInput("ranges",
                    "Select time range:",
                    round = -2,
                    min = round(min(data$time_h), 2),
                    max = round(max(data$time_h), 2),
                    value = c(
                      round(min(data$time_h), 2), 
                      round(max(data$time_h), 2)
                    )
        ),
        helpText("Note: you can also select the time range on a plot. Click and drag, then double click on selection.")
      )
    }
  })
  
  ## Show list of channels when user wants to select channels
  output$channel.filter <- renderUI({
    data <- data.tb()
    if (nrow(data) > 0 & input$channel.select) {
      selectInput("channel", 
                  "Select a channel:",
                  choices = unique(data$channel),
                  multiple = TRUE)
    } 
  })
  
  ## Show list of LEDS hwne there is data available
  output$od.led.filter <- renderUI({  
    data <- data.tb()
    if (nrow(data) > 0) {
      selectInput(inputId = "od.led.select",
                  label = "Only show OD values measured at",
                  choices = unique(data$od_led),
                  selected = last(unique(data$od_led)),
                  multiple = TRUE) 
    }
  })
  
  ## Download button
  output$data.download <- renderUI({
    ## Only show the button if there is data to download ;)
    if (nrow(data.tb()) > 0) {
      tagList(
        downloadButton('downloadRAW', 'Download data'),
        #downloadButton('downloadCSV', 'Download as CSV'),
        downloadButton('downloadGrowthCSV', 'Download growth rates')
      )
    }
  })
  ## Download source file
  output$downloadRAW <- downloadHandler(
    filename = function() { basename(experiment()$path) },
    content = function(file) {
      # copy source file
      file.copy(experiment()$path, file, overwrite=TRUE)
    },
    contentType = "application/x-sqlite3"
  )
  ## Download CSV handler
  output$downloadCSV <- downloadHandler(
    filename = function() { 
      paste(basename(experiment()$path), ".csv", sep="") 
    },
    content = function(file) {
      # write csv file
      write_csv(data.tb(), file)
    },
    contentType = "text/csv"
  )
  
  output$downloadGrowthCSV <- downloadHandler(
    filename = function() { 
      paste(basename(experiment()$path), "_growth.csv", sep="")
    },
    content = function(file) {
      write_csv(data.growth(), file)
    },
    contentType = "text/csv"
  )
  
  ## PLOTS
  
  ## Show a plot with OD values
  output$od.plot <- renderPlot({
    filters <- list(
      xlim = xlim(),
      xlim.zoom = xlim.zoom(),
      filter.channel = input$channel.select == TRUE,
      channels = if(input$channel.select == TRUE) as.list(channel()) else as.list(NA),
      od.led = od.led(),
      od.raw = od.raw(),
      od.log = od.log()
    )
    create.plot.od( data.tb(), filters)
  })
  
  ## Show a plot with the ratio
  output$od.ratio.plot <- renderPlot({
    
    validate( need( 680 %in% unique(data.tb()$od_led), "Missing OD data measured at 680nm" ))
    validate( need( 720 %in% unique(data.tb()$od_led), "Missing OD data measured at 720nm" ))
    
    filters <- list(
      xlim = xlim(),
      xlim.zoom = xlim.zoom(),
      filter.channel = input$channel.select == TRUE,
      channels = if(input$channel.select == TRUE) as.list(channel()) else as.list(NA),
      od.led = od.led(),
      od.raw = od.raw(),
      od.log = od.log()
    )
    
    create.plot.od.ratio( data.tb(), filters)
  })
  
  output$light.plot <- renderPlot({
    filters <- list(
      xlim = xlim(),
      xlim.zoom = xlim.zoom(),
      filter.channel = input$channel.select == TRUE,
      channels = if(input$channel.select == TRUE) as.list(channel()) else as.list(NA)
    )
    
    create.plot.light( data.tb(), filters)
  })
  
  output$light.od.plot <- renderPlot({
    filters <- list(
      xlim = xlim(),
      xlim.zoom = xlim.zoom(),
      filter.channel = input$channel.select == TRUE,
      channels = if(input$channel.select == TRUE) as.list(channel()) else as.list(NA),
      od.raw = od.raw(),
      od.log = od.log()
    )
    
    create.plot.light.od( data.tb(), filters)
  })
  
  output$growth.plot <- renderPlot({
    filters <- list(
      xlim = xlim(),
      xlim.zoom = xlim.zoom(),
      filter.channel = input$channel.select == TRUE,
      channels = if(input$channel.select == TRUE) as.list(channel()) else as.list(NA)
      # od.raw = od.raw(),
      # od.log = od.log()
    )
    
    create.plot.growth( data.growth(), filters)
  })
  
  output$growth.size.plot <- renderPlot({
    filters <- list(
      xlim = xlim(),
      xlim.zoom = xlim.zoom(),
      filter.channel = input$channel.select == TRUE,
      channels = if(input$channel.select == TRUE) as.list(channel()) else as.list(NA)
      # od.raw = od.raw(),
      # od.log = od.log()
    )
    
    create.plot.growth.size( data.growth(), filters)
  })
  
  output$growth.generations.plot <- renderPlot({
    filters <- list(
      xlim = xlim(),
      xlim.zoom = xlim.zoom(),
      filter.channel = input$channel.select == TRUE,
      channels = if(input$channel.select == TRUE) as.list(channel()) else as.list(NA)
      # od.raw = od.raw(),
      # od.log = od.log()
    )
    
    create.plot.generations( data.growth(), filters)
  })
  
  output$dilution.plot <- renderPlot({
    filters <- list(
      xlim = xlim(),
      xlim.zoom = xlim.zoom(),
      filter.channel = input$channel.select == TRUE,
      channels = if(input$channel.select == TRUE) as.list(channel()) else as.list(NA)
      # od.raw = od.raw(),
      # od.log = od.log()
    )
    
    create.plot.dilutions.rate( data.dilution(), filters)
  })
  
  output$dilution.generations.plot <- renderPlot({
    filters <- list(
      xlim = xlim(),
      xlim.zoom = xlim.zoom(),
      filter.channel = input$channel.select == TRUE,
      channels = if(input$channel.select == TRUE) as.list(channel()) else as.list(NA)
      # od.raw = od.raw(),
      # od.log = od.log()
    )
    
    create.plot.dilutions.generations( data.dilution(), filters)
  })
  
  output$pump.rate.plot <- renderPlot({
    filters <- list(
      xlim = xlim(),
      xlim.zoom = xlim.zoom(),
      filter.channel = input$channel.select == TRUE,
      channels = if(input$channel.select == TRUE) as.list(channel()) else as.list(NA)
      # od.raw = od.raw(),
      # od.log = od.log()
    )
    
    create.plot.pump.rate( data.pump() , filters)
  })
  
  output$pump.volume.plot <- renderPlot({
    filters <- list(
      xlim = xlim(),
      xlim.zoom = xlim.zoom(),
      filter.channel = input$channel.select == TRUE,
      channels = if(input$channel.select == TRUE) as.list(channel()) else as.list(NA)
      # od.raw = od.raw(),
      # od.log = od.log()
    )
    
    create.plot.pump.volume( data.pump() , filters)
  })
  
  output$pump.volume.changes.plot <- renderPlot({
    filters <- list(
      xlim = xlim(),
      xlim.zoom = xlim.zoom(),
      filter.channel = input$channel.select == TRUE,
      channels = if(input$channel.select == TRUE) as.list(channel()) else as.list(NA)
      # od.raw = od.raw(),
      # od.log = od.log()
    )

    data.pump() %>%
      create.plot.pump.volume.changes(filters)
  })
  
  output$output.data.table <- renderDataTable({
    dataset <- input$output.data.name
    
    df <- switch (dataset,
      "OD" = data.tb(),
      "Growth" = data.growth(),
      "Pump" = data.dilution(),
      data.frame()
    )

    df
  })
  
  output$summary.text <- renderText({
    validate(need(nrow(data.tb()) > 0, "No data to show"))
    
    t <- data.tb() %>% pull(time)
    t_h <- data.tb() %>% pull(time_h)
    
    sprintf(
      "<div class='well shiny-text-output'>
        Time of first Measurement: <i>%s</i> <br/>
        Time of last Measurement: <i>%s</i> (%s hrs. since first) <br/>
      </div>", 
      format(readr::parse_datetime(as.character(min(t))), "%Y-%m-%d %H:%M:%S"),
      format(readr::parse_datetime(as.character(max(t))), "%Y-%m-%d %H:%M:%S"),
      round(as.numeric(max(t_h)), digits=2)
    )
  })
  
  output$version <- renderText({
    shiny_version <- ""
    
    try(
      shiny_version <- system(
        'shiny-server --version', 
        intern=T, ignore.stderr = T
      ), 
      silent=T
    )
    
    if (!is.na(shiny_version) && shiny_version == 127) {
      shiny_version <- ""
    }
    
    sprintf(
      "<div class='shiny-text-output'>
        <p>App Version: <i>%s</i></p>
        <p>MMPR Version: <i>%s</i></p>
        <p>Shiny Version: <i>%s</i></p>
        <p><i>%s</i></p>
      </div>", 
      APP_VERSION,
      packageVersion("mmpr"),
      packageVersion("shiny"),
      paste(shiny_version, sep=" ", collapse=" ")
    )
  })
  
  #
  # Clean up
  #
  
  # When the client ends the session, suspend the observer.
  # Otherwise, the observer could keep running after the client
  # ends the session.
  session$onSessionEnded(function() {
    #obs.growth.window.start$suspend()
    #obs.growth.window.end$suspend()
  })
  
})

